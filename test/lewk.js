// We import Chai to use its asserting functions here.
const {
  BN,
  constants,
  expectRevert,
} = require('@openzeppelin/test-helpers')
const { expect } = require('chai')
const { ZERO_ADDRESS } = constants

describe('LEWK BEP20 Contract', function () {
  let LEWK
  let initialHolder
  let recipient
  let anotherAccount

  const name = 'LEWK'
  const symbol = 'LEWK'
  const initialSupply = new BN(100)

  // `beforeEach` will run before each test, re-deploying the contract every
  // time. It receives a callback, which can be async.
  beforeEach(async function () {
    // Get the ContractFactory and Signers here.
    ;[
      initialHolder,
      recipient,
      anotherAccount,
      ...addrs
    ] = await ethers.getSigners()

    // Loading the LEWK contract before deploying.
    LEWK = await ethers.getContractFactory('LEWK')

    // Deploy LEWK.
    this.token = await LEWK.deploy([100])
  })

  // You can nest describe calls to create subsections.
  describe('LEWK token initialize variable testing', function () {
    it('has a name', async function () {
      expect(await this.token.name()).to.equal(name)
    })

    it('has a symbol', async function () {
      expect(await this.token.symbol()).to.equal(symbol)
    })

    it('has 18 decimals', async function () {
      expect(await this.token.decimals()).to.equal(18)
    })

    it('returns the total amount of tokens', async function () {
        expect(await this.token.totalSupply()).to.be.equal(100);
      });
  })

  describe('balanceOf', function () {
    describe('when the requested account has no tokens', function () {
      it('returns zero', async function () {
        expect(await this.token.balanceOf(anotherAccount.address)).to.be.equal(0);
      });
    });

    describe('when the requested account has some tokens', function () {
      it('returns the total amount of tokens', async function () {
        expect(await this.token.balanceOf(initialHolder.address)).to.be.equal(100);
      });
    });
  });

  describe('transfer', function () {
      it('Failed if not sufficient balance', async function () {
        await expectRevert(this.token.connect(recipient).transfer(initialHolder.address, "10"), 'ERC20: transfer amount exceeds balance',
        );
      });

      it('Send tokens', async function () {
        await this.token.transfer(recipient.address, "10");
        expect(await this.token.balanceOf(recipient.address)).to.be.equal(10);
        expect(await this.token.balanceOf(initialHolder.address)).to.be.equal(90);
      });
  });

  describe('transferFrom', function () {
    it('Failed if not sufficient allowance', async function () {
      await expectRevert(this.token.transferFrom(initialHolder.address, recipient.address, "10"), 'ERC20: transfer amount exceeds allowance',
      );
    });

    it('Failed if not sufficient balance', async function () {
      await this.token.approve(recipient.address, "1000");
      await expectRevert(this.token.transferFrom(initialHolder.address, recipient.address, "102"), 'ERC20: transfer amount exceeds balance',
      );
    });

    it('Send tokens', async function () {
      await this.token.approve(recipient.address, "1000");
      await this.token.connect(recipient).transferFrom(initialHolder.address, recipient.address, "10");
      expect(await this.token.balanceOf(recipient.address)).to.be.equal(10);
      expect(await this.token.balanceOf(initialHolder.address)).to.be.equal(90);
    });
});

  describe('_burn', function () {
    describe('for a non zero account', function () {
      it('rejects burning more than balance', async function () {
        await expectRevert(this.token.burn("101"), 'ERC20: burn amount exceeds balance',
        );
      });

      it('Can not burn non-admin user', async function () {
        await expectRevert(this.token.connect(recipient).burn("101"), 'Ownable: caller is not the owner',
        );
      });

      it('Can not burnFrom non-admin user', async function () {
        await expectRevert(this.token.connect(recipient).burnFrom(initialHolder.address, "101"), 'Ownable: caller is not the owner',
        );
      });

      const describeBurn = function (description, amount) {
        describe(description, function () {
          beforeEach('burning', async function () {
            this.receipt = await this.token.burn(amount);
          });

          it('decrements totalSupply', async function () {
            const expectedSupply = initialSupply-amount;
            expect(await this.token.totalSupply()).to.be.equal(expectedSupply);
          });

          it('decrements initialHolder balance', async function () {
            const expectedBalance = initialSupply-amount;
            expect(await this.token.balanceOf(initialHolder.address)).to.be.equal(expectedBalance);
          });
        });
      };

      describeBurn('for entire balance', "100");
      describeBurn('for less amount than balance', "99");
    });
  });

  describe('decrease allowance', function () {
    describe('when the spender is not the zero address', function () {
      function shouldDecreaseApproval(amount) {
        describe('when there was no approved amount before', function () {
          it('reverts when there was no approved amount before', async function () {
            await expectRevert(
              this.token.decreaseAllowance(recipient.address, amount, {
                from: initialHolder.address,
              }),
              'ERC20: decreased allowance below zero',
            )
          })
        })

        describe('when the spender had an approved amount', function () {
          const approvedAmount = amount

          beforeEach(async function () {
            await this.token.approve(recipient.address, approvedAmount, {
              from: initialHolder.address,
            })
          })

          it('decreases the spender allowance subtracting the requested amount', async function () {
            await this.token.decreaseAllowance(
              recipient.address,
              approvedAmount-1,
              { from: initialHolder.address },
            )

            expect(
              await this.token.allowance(
                initialHolder.address,
                recipient.address,
              ),
            ).to.be.equal(1)
          })

          it('sets the allowance to zero when all allowance is removed', async function () {
            await this.token.decreaseAllowance(
              recipient.address,
              approvedAmount,
              {
                from: initialHolder.address,
              },
            )
            expect(
              await this.token.allowance(
                initialHolder.address,
                recipient.address,
              ),
            ).to.be.equal(0)
          })

          it('reverts when more than the full allowance is removed', async function () {
            await expectRevert(
              this.token.decreaseAllowance(
                recipient.address,
                approvedAmount+1,
                {
                  from: initialHolder.address,
                },
              ),
              'ERC20: decreased allowance below zero',
            )
          })
        })
      }

      describe('when the sender has enough balance', function () {
        const amount = '100'

        shouldDecreaseApproval(amount)
      })

      describe('when the sender does not have enough balance', function () {
        const amount = '101'

        shouldDecreaseApproval(amount)
      })
    })

    describe('when the spender is the zero address', function () {
      const amount = "100"
      const spender = ZERO_ADDRESS

      it('reverts', async function () {
        await expectRevert(
          this.token.decreaseAllowance(recipient.address, amount, {
            from: initialHolder.address,
          }),
          'ERC20: decreased allowance below zero',
        )
      })
    })
  })

  describe('increase allowance', function () {
    const amount = "100"

    describe('when the spender is not the zero address', function () {
      describe('when the sender has enough balance', function () {
        describe('when there was no approved amount before', function () {
          it('approves the requested amount', async function () {
            await this.token.increaseAllowance(recipient.address, "100", {
              from: initialHolder.address,
            })

            expect(
              await this.token.allowance(
                initialHolder.address,
                recipient.address,
              ),
            ).to.be.equal(100)
          })
        })

        describe('when the spender had an approved amount', function () {
          beforeEach(async function () {
            await this.token.approve(recipient.address, "1", {
              from: initialHolder.address,
            })
          })

          it('increases the spender allowance adding the requested amount', async function () {
            await this.token.increaseAllowance(recipient.address, amount, {
              from: initialHolder.address,
            })

            expect(
              await this.token.allowance(
                initialHolder.address,
                recipient.address,
              ),
            ).to.be.equal(101)
          })
        })
      })

      describe('when the sender does not have enough balance', function () {
        const amount = "101"

        describe('when there was no approved amount before', function () {
          it('approves the requested amount', async function () {
            await this.token.increaseAllowance(recipient.address, "101", {
              from: initialHolder.address,
            })

            expect(
              await this.token.allowance(
                initialHolder.address,
                recipient.address,
              ),
            ).to.be.equal(101)
          })
        })

        describe('when the spender had an approved amount', function () {
          beforeEach(async function () {
            await this.token.approve(recipient.address, "1", {
              from: initialHolder.address,
            })
          })

          it('increases the spender allowance adding the requested amount', async function () {
            await this.token.increaseAllowance(recipient.address, amount, {
              from: initialHolder.address,
            })

            expect(
              await this.token.allowance(
                initialHolder.address,
                recipient.address,
              ),
            ).to.be.equal(102)
          })
        })
      })
    })

    describe('when the spender is the zero address', function () {
      const spender = ZERO_ADDRESS

      it('reverts', async function () {
        await expectRevert(
          this.token.increaseAllowance(spender, amount, {
            from: initialHolder.address,
          }),
          'ERC20: approve to the zero address',
        )
      })
    })
  })
})
