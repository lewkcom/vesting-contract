const { ethers, upgrades } = require('hardhat')

async function main() {
  // Get the Address from Ganache Chain to deploy.
  const [deployer] = await ethers.getSigners()
  console.log('Deployer address', deployer.address)

  // Loading the contract before deploying.
  const tokenContract = await ethers.getContractFactory('LEWK')
  const token = await tokenContract.deploy(
    ethers.utils.parseUnits('1000000000'),
  )
  await token.deployed()
  console.log('Standard LEWK Token deployed to:', token.address)

  // Loading the contract before deploying.
  const vestingContract = await ethers.getContractFactory('TokenVesting')
  // Deploy proxy.
  const vestingProxyContract = await vestingContract.deploy(token.address, 60, 5, 120, 120, 5, 1000)
  // Waiting till the transaction is completed.
  await vestingProxyContract.deployed();

  // Print the addresses
  console.log(
    'vesting contract is deployed successfully.',
    '\n',
    'Proxy contract address:',
    vestingProxyContract.address,
  )
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })

