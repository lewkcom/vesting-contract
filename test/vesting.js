// We import Chai to use its asserting functions here.
const { BN, constants, expectRevert } = require('@openzeppelin/test-helpers')
const { expect } = require('chai')
const { ZERO_ADDRESS } = constants

describe('Vesting Contract', function () {
  let LEWK
  let Vesting
  let vestingProxyContract
  let initialHolder
  let recipient
  let anotherAccount1
  let anotherAccount2

  // `beforeEach` will run before each test, re-deploying the contract every
  // time. It receives a callback, which can be async.
  beforeEach(async function () {
    // Get the ContractFactory and Signers here.
    ;[
      initialHolder,
      recipient,
      anotherAccount1,
      anotherAccount2,
      ...addrs
    ] = await ethers.getSigners()

    // Loading the LEWK contract before deploying.
    LEWK = await ethers.getContractFactory('LEWK')
    Vesting = await ethers.getContractFactory('MockTokenVesting')

    // Deploy LEWK.
    this.token = await LEWK.deploy(ethers.utils.parseUnits('1000000000'))
  })

  // You can nest describe calls to create subsections.
  describe('Vesting Contract initialize variable testing', function () {
    beforeEach('Deploying Contract', async function () {
      vestingProxyContract = await Vesting.deploy(
        this.token.address,
        60,
        5,
        120,
        120,
        5,
        860
      )
    })

    it('Correct vesting token', async function () {
      expect(await vestingProxyContract.token()).to.equal(this.token.address)
    })

    it('Correct owner', async function () {
      expect(await vestingProxyContract.owner()).to.equal(initialHolder.address)
    })

    it('Correct tgeTime', async function () {
      expect(await vestingProxyContract.tgeTime()).to.equal(60)
    })

    it('Correct tgePercent', async function () {
      expect(await vestingProxyContract.tgePercent()).to.equal(5)
    })

    it('Correct cliffTime', async function () {
      expect(await vestingProxyContract.cliffTime()).to.equal(120)
    })

    it('Correct secondDistributionTime', async function () {
      expect(await vestingProxyContract.secondDistributionTime()).to.equal(120)
    })

    it('Correct secondDistributionPercent', async function () {
      expect(await vestingProxyContract.secondDistributionPercent()).to.equal(5)
    })

    it('Correct completeVestingPeriod', async function () {
      expect(await vestingProxyContract.completeVestingPeriod()).to.equal(860)
    })

    it('Correct monthPeriod', async function () {
      expect(await vestingProxyContract.monthPeriod()).to.equal(120)
    })

    it('Correct totalVestingTokens value', async function () {
      expect(await vestingProxyContract.totalVestingTokens()).to.equal(0)
    })
  })

  describe('whiteListAddresses', function () {
    beforeEach('Deploying Contract', async function () {
      vestingProxyContract = await Vesting.deploy(
        this.token.address,
        60,
        5,
        120,
        120,
        5,
        860
      )
    })

    it('Failed if non-admin try to whitelist users', async function () {
      await expectRevert(
        vestingProxyContract
          .connect(recipient)
          .whiteListAddresses(
            [anotherAccount1.address, anotherAccount2.address],
            [100, 200],
          ),
        'Ownable: caller is not the owner',
      )
    })

    it('Failed if owner have not enough tokens', async function () {
      await vestingProxyContract.transferOwnership(recipient.address)
      await expectRevert(
        vestingProxyContract
          .connect(recipient)
          .whiteListAddresses(
            [anotherAccount1.address, anotherAccount2.address],
            [100, 200],
            { from: recipient.address },
          ),
        'ERC20: transfer amount exceeds balance',
      )
    })

    it('Failed if owner try to whitelist zero address', async function () {
      await expectRevert(
        vestingProxyContract.whiteListAddresses(
          [anotherAccount1.address, ZERO_ADDRESS],
          [100, 200],
        ),
        'Invalid address',
      )
    })

    it('Failed if vesting already start', async function () {
      await vestingProxyContract.startVesting()
      await expectRevert(
        vestingProxyContract.whiteListAddresses(
          [anotherAccount1.address, anotherAccount2.address],
          [100, 200],
        ),
        'Vesting already start',
      )
    })

    it('Failed if address and amount with invalid data', async function () {
      await expectRevert(
        vestingProxyContract.whiteListAddresses(
          [anotherAccount1.address, anotherAccount2.address],
          [200],
        ),
        'Invalid data',
      )
      await expectRevert(
        vestingProxyContract.whiteListAddresses(
          [anotherAccount2.address],
          [100, 200],
        ),
        'Invalid data',
      )
    })

    it('Failed if owner not approved enough token amount', async function () {
      await expectRevert(
        vestingProxyContract.whiteListAddresses(
          [anotherAccount1.address, anotherAccount2.address],
          [200, 200],
        ),
        'ERC20: transfer amount exceeds allowance',
      )
    })

    it('Admin can whitelist the address', async function () {
      await this.token.approve(vestingProxyContract.address, 300, {
        from: initialHolder.address,
      })
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
    })

    it('Vesting contract receive all tokens', async function () {
      await this.token.approve(vestingProxyContract.address, 300, {
        from: initialHolder.address,
      })
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )

      expect(await this.token.balanceOf(vestingProxyContract.address)).to.equal(
        300,
      )
    })

    it('totalVestingTokens value increase', async function () {
      await this.token.approve(vestingProxyContract.address, 300, {
        from: initialHolder.address,
      })
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )

      expect(await vestingProxyContract.totalVestingTokens()).to.equal(300)
    })
  })

  describe('Initialize currect locked value', function () {
    beforeEach('Deploying Contract', async function () {
      vestingProxyContract = await Vesting.deploy(
        this.token.address,
        60,
        5,
        120,
        120,
        5,
        860
      )
      await this.token.approve(vestingProxyContract.address, 300, {
        from: initialHolder.address,
      })
    })

    it('User1', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      let info = await vestingProxyContract.locked(anotherAccount1.address)

      const blockNumBefore = await ethers.provider.getBlockNumber()
      const blockBefore = await ethers.provider.getBlock(blockNumBefore)
      const timestampBefore = blockBefore.timestamp

      expect(info[0]).to.equal(100)
      expect(info[1]).to.equal(timestampBefore)
      expect(info[2]).to.equal(0)
      expect(info[3]).to.equal(timestampBefore)
      expect(info[4]).to.equal(false)
      expect(info[5]).to.equal(false)
    })

    it('User2', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      let info = await vestingProxyContract.locked(anotherAccount2.address)

      const blockNumBefore = await ethers.provider.getBlockNumber()
      const blockBefore = await ethers.provider.getBlock(blockNumBefore)
      const timestampBefore = blockBefore.timestamp

      expect(info[0]).to.equal(200)
      expect(info[1]).to.equal(timestampBefore)
      expect(info[2]).to.equal(0)
      expect(info[3]).to.equal(timestampBefore)
      expect(info[4]).to.equal(false)
      expect(info[5]).to.equal(false)
    })
  })

  describe('Listout all users', function () {
    beforeEach('Deploying Contract', async function () {
      vestingProxyContract = await Vesting.deploy(
        this.token.address,
        60,
        5,
        120,
        120,
        5,
        860
      )

      await this.token.approve(vestingProxyContract.address, 300, {
        from: initialHolder.address,
      })
    })

    it('All Users List', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      expect(await vestingProxyContract.whiteListUsers(0)).to.equal(
        anotherAccount1.address
      )
      expect(await vestingProxyContract.whiteListUsers(1)).to.equal(
        anotherAccount2.address
      )
    })
  })

  describe('TGE Period', function () {
    beforeEach('Deploying Contract', async function () {
      vestingProxyContract = await Vesting.deploy(
        this.token.address,
        60,
        5,
        120,
        120,
        5,
        860
      )

      await this.token.approve(vestingProxyContract.address, 300, {
        from: initialHolder.address,
      })
    })

    it('Revert if vesting not start', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await expectRevert(
        vestingProxyContract
          .connect(anotherAccount2)
          .unlock(anotherAccount2.address, { from: anotherAccount2.address }),
        'Vesting not start',
      )
    })

    it('Recieve 0 token before TGE Time', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      let unlockable1 = await vestingProxyContract.getUnlockableTokens(anotherAccount1.address)
      let unlockable2 = await vestingProxyContract.getUnlockableTokens(anotherAccount2.address)

      expect(unlockable1).to.equal(0)
      expect(unlockable2).to.equal(0)

      let beforeBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let beforeBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(beforeBalanceWallet1).to.equal(afterBalanceWallet1)
      expect(beforeBalanceWallet2).to.equal(afterBalanceWallet2)

      let unlockable11 = await vestingProxyContract.getUnlockableTokens(anotherAccount1.address)
      let unlockable12 = await vestingProxyContract.getUnlockableTokens(anotherAccount2.address)

      expect(unlockable11).to.equal(0)
      expect(unlockable12).to.equal(0)
    })

    it('Recieve 5% token after TGE Time', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [61])
      await ethers.provider.send('evm_mine')

      let unlockable1 = await vestingProxyContract.getUnlockableTokens(anotherAccount1.address)
      let unlockable2 = await vestingProxyContract.getUnlockableTokens(anotherAccount2.address)

      expect(unlockable1).to.equal(5)
      expect(unlockable2).to.equal(10)

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1).to.equal(5) // 5% of 100
      expect(afterBalanceWallet2).to.equal(10) // 5% of 200

      let unlockable11 = await vestingProxyContract.getUnlockableTokens(anotherAccount1.address)
      let unlockable12 = await vestingProxyContract.getUnlockableTokens(anotherAccount2.address)

      expect(unlockable11).to.equal(0)
      expect(unlockable12).to.equal(0)
    })

    it('Not Recieve again 5% token after TGE Time', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [61])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1).to.equal(5) // 5% of 100
      expect(afterBalanceWallet2).to.equal(10) // 5% of 200

      await ethers.provider.send('evm_increaseTime', [11])
      await ethers.provider.send('evm_mine')

      let unlockable1 = await vestingProxyContract.getUnlockableTokens(anotherAccount1.address)
      let unlockable2 = await vestingProxyContract.getUnlockableTokens(anotherAccount2.address)

      expect(unlockable1).to.equal(0)
      expect(unlockable2).to.equal(0)

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet11 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet12 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet11).to.equal(5) // 5% of 100
      expect(afterBalanceWallet12).to.equal(10) // 5% of 200
    })

    it('Admin can update TGE time before TGE time end', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()
      await vestingProxyContract.updateTgeConfig(0, 70)

      await ethers.provider.send('evm_increaseTime', [61])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1).to.equal(0)
      expect(afterBalanceWallet2).to.equal(0)

      await ethers.provider.send('evm_increaseTime', [11])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet11 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet12 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet11).to.equal(5) // 5% of 100
      expect(afterBalanceWallet12).to.equal(10) // 5% of 200
    })

    it('Revert if Admin try to update TGE time after TGE time', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [61])
      await ethers.provider.send('evm_mine')

      await expectRevert(
        vestingProxyContract.updateTgeConfig(0, 70),
        'TGE already done',
      )
    })

    it('Admin can update TGE percent before TGE time end', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()
      await vestingProxyContract.updateTgeConfig(6, 0)

      await ethers.provider.send('evm_increaseTime', [61])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1).to.equal(6) // 6% of 100
      expect(afterBalanceWallet2).to.equal(12) // 6% of 200

      await ethers.provider.send('evm_increaseTime', [11])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet11 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet12 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet11).to.equal(6) // 6% of 100
      expect(afterBalanceWallet12).to.equal(12) // 6% of 200
    })

    it('Revert if Admin try to update TGE percent after TGE time', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [61])
      await ethers.provider.send('evm_mine')

      await expectRevert(
        vestingProxyContract.updateTgeConfig(6, 0),
        'TGE already done',
      )
    })
  })

  describe('Month Period', function () {
    beforeEach('Deploying Contract', async function () {
      vestingProxyContract = await Vesting.deploy(
        this.token.address,
        59,
        5,
        1,
        120,
        5,
        860
      )

      await this.token.approve(vestingProxyContract.address, 300, {
        from: initialHolder.address,
      })
    })

    it('Recieve 0 token in rest TGE Time(After TGE)', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [61])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1).to.equal(5) // 5% of 100
      expect(afterBalanceWallet2).to.equal(10) // 5% of 200

      await ethers.provider.send('evm_increaseTime', [11])
      await ethers.provider.send('evm_mine')

      let unlockable1 = await vestingProxyContract.getUnlockableTokens(anotherAccount1.address)
      let unlockable2 = await vestingProxyContract.getUnlockableTokens(anotherAccount2.address)

      expect(unlockable1).to.equal(0)
      expect(unlockable2).to.equal(0)

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet11 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet12 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet11).to.equal(5) // 5% of 100
      expect(afterBalanceWallet12).to.equal(10) // 5% of 200
    })

    it('Start Recieving tokens after TGE Time(After TGE)', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [141])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1 > 0).to.equal(true)
      expect(afterBalanceWallet2 > 0).to.equal(true)
    })
  })

  describe('Cliff Period', function () {
    beforeEach('Deploying Contract', async function () {
      vestingProxyContract = await Vesting.deploy(
        this.token.address,
        60,
        5,
        120,
        120,
        5,
        860
      )

      await this.token.approve(vestingProxyContract.address, 300, {
        from: initialHolder.address,
      })
    })

    it('Recieve 0 token in Cliff Time', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [61])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1).to.equal(5) // 5% of 100
      expect(afterBalanceWallet2).to.equal(10) // 5% of 200

      let unlockable1 = await vestingProxyContract.getUnlockableTokens(anotherAccount1.address)
      let unlockable2 = await vestingProxyContract.getUnlockableTokens(anotherAccount2.address)

      expect(unlockable1).to.equal(0)
      expect(unlockable2).to.equal(0)

      await ethers.provider.send('evm_increaseTime', [179])
      await ethers.provider.send('evm_mine')

      let unlockable11 = await vestingProxyContract.getUnlockableTokens(anotherAccount1.address)
      let unlockable12 = await vestingProxyContract.getUnlockableTokens(anotherAccount2.address)

      expect(unlockable11).to.equal(0)
      expect(unlockable12).to.equal(0)

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet11 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet12 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet11).to.equal(5) // 5% of 100
      expect(afterBalanceWallet12).to.equal(10) // 5% of 200
    })

    it('Start Recieving tokens in after Cliff Time', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [61])
      await ethers.provider.send('evm_mine')

      let unlockable1 = await vestingProxyContract.getUnlockableTokens(anotherAccount1.address)
      let unlockable2 = await vestingProxyContract.getUnlockableTokens(anotherAccount2.address)

      expect(unlockable1).to.equal(5)
      expect(unlockable2).to.equal(10)

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1).to.equal(5) // 5% of 100
      expect(afterBalanceWallet2).to.equal(10) // 5% of 200

      let unlockable11 = await vestingProxyContract.getUnlockableTokens(anotherAccount1.address)
      let unlockable12 = await vestingProxyContract.getUnlockableTokens(anotherAccount2.address)

      expect(unlockable11).to.equal(0)
      expect(unlockable12).to.equal(0)

      await ethers.provider.send('evm_increaseTime', [179])
      await ethers.provider.send('evm_mine')

      let unlockable111 = await vestingProxyContract.getUnlockableTokens(anotherAccount1.address)
      let unlockable112 = await vestingProxyContract.getUnlockableTokens(anotherAccount2.address)

      expect(unlockable111).to.equal(0)
      expect(unlockable112).to.equal(0)

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet11 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet12 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet11).to.equal(5) // 5% of 100
      expect(afterBalanceWallet12).to.equal(10) // 5% of 200

      await ethers.provider.send('evm_increaseTime', [60])
      await ethers.provider.send('evm_mine')

      let unlockable1111 = await vestingProxyContract.getUnlockableTokens(anotherAccount1.address)
      let unlockable1112 = await vestingProxyContract.getUnlockableTokens(anotherAccount2.address)

      expect(unlockable1111).to.equal(2)
      expect(unlockable1112).to.equal(5)

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet121 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet122 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet121).to.equal(7) // 5+2.5% of 100
      expect(afterBalanceWallet122).to.equal(15) // 5+2.5% of 200
    })

    it('Admin can update Cliff time before Cliff time end', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()
      await vestingProxyContract.updateCliffConfig(70)

      await ethers.provider.send('evm_increaseTime', [61])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1).to.equal(5)
      expect(afterBalanceWallet2).to.equal(10)

      await ethers.provider.send('evm_increaseTime', [129])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet11 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet12 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet11).to.equal(5) // 5% of 100
      expect(afterBalanceWallet12).to.equal(10) // 5% of 200

      await ethers.provider.send('evm_increaseTime', [60])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet121 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet122 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet121).to.equal(7) // 5+2.5% of 100
      expect(afterBalanceWallet122).to.equal(15) // 5+2.5% of 200
    })

    it('Revert if Admin try to update Cliff time after Cliff time', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [240])
      await ethers.provider.send('evm_mine')

      await expectRevert(
        vestingProxyContract.updateCliffConfig(70),
        'Cliff already done',
      )
    })
  })

  describe('Secondary Distribution Period', function () {
    beforeEach('Deploying Contract', async function () {
      vestingProxyContract = await Vesting.deploy(
        this.token.address,
        60,
        5,
        120,
        120,
        5,
        860
      )

      await this.token.approve(vestingProxyContract.address, 300, {
        from: initialHolder.address,
      })
    })

    it('Recieve 0 token before Second distribution Time', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [240])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1).to.equal(5) // 5% of 100
      expect(afterBalanceWallet2).to.equal(10) // 5% of 200
    })

    it('Recieve Linearly 5% token in Second distribution Time', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [240])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1).to.equal(5) // 5% of 100
      expect(afterBalanceWallet2).to.equal(10) // 5% of 200

      await ethers.provider.send('evm_increaseTime', [30])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet11 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet12 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet11).to.equal(6)
      expect(afterBalanceWallet12).to.equal(12)

      await ethers.provider.send('evm_increaseTime', [30])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet111 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet112 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet111).to.equal(7)
      expect(afterBalanceWallet112).to.equal(15)
    })

    it('Recieve complete 5% token in Second distribution Time', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [360])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1).to.equal(10) // 10% of 100
      expect(afterBalanceWallet2).to.equal(20) // 10% of 200
    })

    it('Admin can update Second distribution time before Second distribution time end', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [200])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract.updateSecondDistributionConfig(60, 0)

      await ethers.provider.send('evm_increaseTime', [100])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1).to.equal(10) // 10% of 100
      expect(afterBalanceWallet2).to.equal(20) // 10% of 200
    })

    it('Revert if Admin try to update Second distribution time after Second distribution time', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [360])
      await ethers.provider.send('evm_mine')

      await expectRevert(
        vestingProxyContract.updateSecondDistributionConfig(70, 0),
        'Second distribution currently running or done',
      )
    })

    it('Admin can update Second distribution percent before Second distribution time end', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()
      await vestingProxyContract.updateSecondDistributionConfig(0, 6)

      await ethers.provider.send('evm_increaseTime', [360])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1).to.equal(11) // 5+6% of 100
      expect(afterBalanceWallet2).to.equal(23) // 5+6% of 200
    })

    it('Revert if Admin try to update Second distribution percent after Second distribution time', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [360])
      await ethers.provider.send('evm_mine')

      await expectRevert(
        vestingProxyContract.updateSecondDistributionConfig(0, 6),
        'Second distribution currently running or done',
      )
    })
  })

  describe('Complete Vesting Period', function () {
    beforeEach('Deploying Contract', async function () {
      vestingProxyContract = await Vesting.deploy(
        this.token.address,
        60,
        5,
        120,
        120,
        5,
        860
      )

      await this.token.approve(vestingProxyContract.address, 300, {
        from: initialHolder.address,
      })
    })

    it('Start Recieving Linearly 90% token after Second distribution Time', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [240])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1).to.equal(5) // 5% of 100
      expect(afterBalanceWallet2).to.equal(10) // 5% of 200

      await ethers.provider.send('evm_increaseTime', [100])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet11 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet12 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet11).to.equal(9)
      expect(afterBalanceWallet12).to.equal(18)

      await ethers.provider.send('evm_increaseTime', [100])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet111 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet112 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet111).to.equal(25)
      expect(afterBalanceWallet112).to.equal(50)
    })

    it('Recieve complete 90% token after Complete vesting Time', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [860])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1).to.equal(100) // 100% of 100
      expect(afterBalanceWallet2).to.equal(200) // 100% of 200
    })

    it('Admin can update complete vesting time before complete vesting time end', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [200])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract.updateCompleteVestingConfig(1000)

      await ethers.provider.send('evm_increaseTime', [700])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1).to.equal(86)
      expect(afterBalanceWallet2).to.equal(172)

      await ethers.provider.send('evm_increaseTime', [100])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet11 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet12 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet11).to.equal(100)
      expect(afterBalanceWallet12).to.equal(200)
    })

    it('Revert if Admin try to update complete vesting time after vesting complete', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [860])
      await ethers.provider.send('evm_mine')

      await expectRevert(
        vestingProxyContract.updateCompleteVestingConfig(1000),
        'Vesting already done',
      )
    })

    it('Admin can update complete vesting time between vesting time end', async function () {
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
      await vestingProxyContract.startVesting()

      await ethers.provider.send('evm_increaseTime', [500])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract.updateCompleteVestingConfig(900)

      await ethers.provider.send('evm_increaseTime', [1500])
      await ethers.provider.send('evm_mine')

      await vestingProxyContract
        .connect(anotherAccount1)
        .unlock(anotherAccount1.address, { from: anotherAccount1.address })
      await vestingProxyContract
        .connect(anotherAccount2)
        .unlock(anotherAccount2.address, { from: anotherAccount2.address })

      let afterBalanceWallet1 = await this.token.balanceOf(
        anotherAccount1.address,
      )
      let afterBalanceWallet2 = await this.token.balanceOf(
        anotherAccount2.address,
      )

      expect(afterBalanceWallet1).to.equal(100) // 100% of 100
      expect(afterBalanceWallet2).to.equal(200) // 100% of 200
    })
  })

  describe('updateTgeConfig', function () {
    beforeEach('Deploying Contract', async function () {
      vestingProxyContract = await Vesting.deploy(
        this.token.address,
        60,
        5,
        120,
        120,
        5,
        860
      )

      await this.token.approve(vestingProxyContract.address, 300, {
        from: initialHolder.address,
      })
      await vestingProxyContract.startVesting()
    })

    it('Failed if non-admin try to updateTgeConfig', async function () {
      await expectRevert(
        vestingProxyContract.connect(recipient).updateTgeConfig(12, 12),
        'Ownable: caller is not the owner',
      )
    })

    it('Only admin can updateTgeConfig', async function () {
      await vestingProxyContract.transferOwnership(recipient.address)
      await vestingProxyContract
        .connect(recipient)
        .updateTgeConfig(12, 20, { from: recipient.address })

      expect(await vestingProxyContract.tgeTime()).to.equal(20)
      expect(await vestingProxyContract.tgePercent()).to.equal(12)
    })
  })

  describe('updateCliffConfig', function () {
    beforeEach('Deploying Contract', async function () {
      vestingProxyContract = await Vesting.deploy(
        this.token.address,
        60,
        5,
        120,
        120,
        5,
        860
      )

      await this.token.approve(vestingProxyContract.address, 300, {
        from: initialHolder.address,
      })
      await vestingProxyContract.startVesting()
    })

    it('Failed if non-admin try to updateCliffConfig', async function () {
      await expectRevert(
        vestingProxyContract.connect(recipient).updateCliffConfig(102),
        'Ownable: caller is not the owner',
      )
    })

    it('Only admin can updateCliffConfig', async function () {
      await vestingProxyContract.transferOwnership(recipient.address)
      await vestingProxyContract
        .connect(recipient)
        .updateCliffConfig(102, { from: recipient.address })

      expect(await vestingProxyContract.cliffTime()).to.equal(102)
    })
  })

  describe('updateSecondDistributionConfig', function () {
    beforeEach('Deploying Contract', async function () {
      vestingProxyContract = await Vesting.deploy(
        this.token.address,
        60,
        5,
        120,
        120,
        5,
        860
      )

      await this.token.approve(vestingProxyContract.address, 300, {
        from: initialHolder.address,
      })
      await vestingProxyContract.startVesting()
    })

    it('Failed if non-admin try to updateTgeConfig', async function () {
      await expectRevert(
        vestingProxyContract
          .connect(recipient)
          .updateSecondDistributionConfig(12, 12),
        'Ownable: caller is not the owner',
      )
    })

    it('Only admin can updateSecondDistributionConfig', async function () {
      await vestingProxyContract.transferOwnership(recipient.address)
      await vestingProxyContract
        .connect(recipient)
        .updateSecondDistributionConfig(12, 20, { from: recipient.address })

      expect(await vestingProxyContract.secondDistributionPercent()).to.equal(
        20,
      )
      expect(await vestingProxyContract.secondDistributionTime()).to.equal(12)
    })
  })

  describe('updateCompleteVestingConfig', function () {
    beforeEach('Deploying Contract', async function () {
      vestingProxyContract = await Vesting.deploy(
        this.token.address,
        60,
        5,
        120,
        120,
        5,
        860
      )

      await this.token.approve(vestingProxyContract.address, 300, {
        from: initialHolder.address,
      })
      await vestingProxyContract.startVesting()
    })

    it('Failed if non-admin try to updateCompleteVestingConfig', async function () {
      await expectRevert(
        vestingProxyContract
          .connect(recipient)
          .updateCompleteVestingConfig(102),
        'Ownable: caller is not the owner',
      )
    })

    it('Only admin can updateCompleteVestingConfig', async function () {
      await vestingProxyContract.transferOwnership(recipient.address)
      await vestingProxyContract
        .connect(recipient)
        .updateCompleteVestingConfig(902, { from: recipient.address })

      expect(await vestingProxyContract.completeVestingPeriod()).to.equal(902)
    })
  })

  describe('startVesting', function () {
    beforeEach('Deploying Contract', async function () {
      vestingProxyContract = await Vesting.deploy(
        this.token.address,
        60,
        5,
        120,
        120,
        5,
        860
      )

      await this.token.approve(vestingProxyContract.address, 300, {
        from: initialHolder.address,
      })
      await vestingProxyContract.whiteListAddresses(
        [anotherAccount1.address, anotherAccount2.address],
        [100, 200],
      )
    })

    it('Failed if non-admin try to start vesting', async function () {
      await expectRevert(
        vestingProxyContract.connect(recipient).startVesting(),
        'Ownable: caller is not the owner',
      )
    })

    it('Only admin can start vesting', async function () {
      await vestingProxyContract.transferOwnership(recipient.address)
      await vestingProxyContract.connect(recipient).startVesting({
        from: recipient.address,
      })

      expect(await vestingProxyContract.vestingStatus()).to.equal(true)
    })

    it('Revert if admin try to start vesting more than one time', async function () {
      await vestingProxyContract.transferOwnership(recipient.address)
      await vestingProxyContract.connect(recipient).startVesting({
        from: recipient.address,
      })

      await expectRevert(
        vestingProxyContract.connect(recipient).startVesting({
          from: recipient.address,
        }),
        'Vesting already started',
      )
    })
  })
})
