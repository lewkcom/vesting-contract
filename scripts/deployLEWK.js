const { ethers } = require('hardhat')

async function main() {
  // Get the Address from Ganache Chain to deploy.
  const [deployer] = await ethers.getSigners()
  console.log('Deployer address', deployer.address)

  // Loading the contract before deploying.
  const tokenContract = await ethers.getContractFactory('LEWK')
  const token = await tokenContract.deploy(
    ethers.utils.parseUnits('1000000000'),
  )
  await token.deployed()

  console.log('StandardToken deployed to:', token.address)
}

main()
