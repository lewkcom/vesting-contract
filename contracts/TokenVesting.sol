// SPDX-License-Identifier: MIT
pragma solidity =0.8.4;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

contract TokenVesting is Ownable {
  using SafeMath for uint256;
  /**
   * @dev Error messages for require statements
   */
  IERC20 public immutable token;
  mapping(address => lockToken) public locked;
  address[] public whiteListUsers;
  uint256 public totalVestingTokens;
  bool public vestingStatus;
  uint256 public vestingStartTime;
  uint256 public tgeTime;
  uint256 public tgePercent;
  uint256 public cliffTime;
  uint256 public secondDistributionTime;
  uint256 public secondDistributionPercent;
  uint256 public completeVestingPeriod;
  uint256 public constant monthPeriod = 2592000; //2592000

  event UpdateTgeConfig(uint256 tgeTime, uint256 tgePercent);
  event UpdateCliffConfig(uint256 cliffTime);
  event UpdateSecondDistributionConfig(
    uint256 secondDistributionTime,
    uint256 secondDistributionPercent
  );
  event UpdateCompleteVestingConfig(uint256 completeVestingPeriod);
  event StartVesting(uint256 time, bool status);
  event EmergencyWithdrawToken(address to, uint256 amount);
  event WhiteListAddresses(uint256 totalUsers, uint256 tokenAmount);
  event Unlock(address to, uint256 amount);

  struct lockToken {
    uint256 amount;
    uint256 lockTime;
    uint256 vestingTokens;
    uint256 lastVestingTime;
    bool tgeClaimed;
    bool totalVested;
  }

  constructor(
    address _token,
    uint256 _tgeTime,
    uint256 _tgePercent,
    uint256 _cliffTime,
    uint256 _secondDistributionTime,
    uint256 _secondDistributionPercent,
    uint256 _completeVestingPeriod
  ) {
    require(_token != address(0), "Invalid token address");
    require(_tgeTime > 0, "Tge time can not be 0");
    require(_tgePercent <= 100, "Tge percent can not be greater than 100");
    require(_cliffTime > 0, "Cliff time can not be 0");
    require(
      _secondDistributionTime > 0,
      "Second distribution time can not be 0"
    );
    require(
      _secondDistributionPercent + _tgePercent <= 100,
      "Second distribution percent is wrong"
    );
    require(
      _completeVestingPeriod >=
        monthPeriod + _cliffTime + _secondDistributionTime,
      "Complete vesting period is too low"
    );

    token = IERC20(_token);
    tgeTime = _tgeTime;
    tgePercent = _tgePercent;
    cliffTime = _cliffTime;
    secondDistributionTime = _secondDistributionTime;
    secondDistributionPercent = _secondDistributionPercent;
    completeVestingPeriod = _completeVestingPeriod;
  }

  /**
   * update tge config
   */
  function updateTgeConfig(uint256 _tgePercent, uint256 _tgeTime)
    external
    onlyOwner
    returns (bool)
  {
    require(
      tgeTime.add(vestingStartTime) >= block.timestamp,
      "TGE already done"
    );
    if (_tgeTime > 0) {
      tgeTime = _tgeTime;
    }
    if (_tgePercent > 0) {
      require(_tgePercent <= 100, "Tge percent can not be greater than 100");
      tgePercent = _tgePercent;
    }

    emit UpdateTgeConfig(_tgeTime, _tgePercent);
    return true;
  }

  /**
   * update cliff config
   */
  function updateCliffConfig(uint256 _cliffTime)
    external
    onlyOwner
    returns (bool)
  {
    if (_cliffTime > 0) {
      require(
        monthPeriod.add(vestingStartTime).add(cliffTime) >= block.timestamp,
        "Cliff already done"
      );
      cliffTime = _cliffTime;
    }

    emit UpdateCliffConfig(_cliffTime);
    return true;
  }

  /**
   * update second distribution config
   */
  function updateSecondDistributionConfig(
    uint256 _secondDistributionTime,
    uint256 _secondDistributionPercent
  ) external onlyOwner returns (bool) {
    require(
      monthPeriod.add(vestingStartTime).add(cliffTime) >= block.timestamp,
      "Second distribution currently running or done"
    );
    if (_secondDistributionTime > 0) {
      secondDistributionTime = _secondDistributionTime;
    }
    if (_secondDistributionPercent > 0) {
      require(
        _secondDistributionPercent + tgePercent <= 100,
        "Second distribution percent is wrong"
      );
      secondDistributionPercent = _secondDistributionPercent;
    }

    emit UpdateSecondDistributionConfig(
      _secondDistributionTime,
      _secondDistributionPercent
    );
    return true;
  }

  /**
   * update complete vesting config
   */
  function updateCompleteVestingConfig(uint256 _completeVestingPeriod)
    external
    onlyOwner
    returns (bool)
  {
    require(
      _completeVestingPeriod >=
        monthPeriod + cliffTime + secondDistributionTime,
      "Complete vesting period is too low"
    );
    require(
      vestingStartTime.add(completeVestingPeriod) >= block.timestamp,
      "Vesting already done"
    );
    completeVestingPeriod = _completeVestingPeriod;

    emit UpdateCompleteVestingConfig(_completeVestingPeriod);
    return true;
  }

  /**
   * Start vesting
   */
  function startVesting() external onlyOwner returns (bool) {
    require(!vestingStatus, "Vesting already started");
    vestingStatus = true;
    vestingStartTime = block.timestamp;

    emit StartVesting(vestingStartTime, vestingStatus);
    return true;
  }

  /**
   * @dev whiteListAddress a specified amount of tokens against an address,
   *      for a specified reason and time
   * @param _amount Number of tokens to be locked
   */
  function whiteListAddresses(address[] memory _to, uint256[] memory _amount)
    external
    onlyOwner
    returns (bool)
  {
    require(!vestingStatus, "Vesting already started");
    uint256 len = _to.length;
    require(len == _amount.length, "Invalid data");
    uint256 nowTime = block.timestamp;
    uint256 previousTokens = totalVestingTokens;
    uint256 tokens = previousTokens;
    for (uint256 i; i < len; i++) {
      require(_to[i] != address(0), "Invalid address");
      require(_amount[i] != 0, "Amount can not be 0");
      uint256 previousAmount = locked[_to[i]].amount;
      if (locked[_to[i]].lockTime == 0) {
        whiteListUsers.push(_to[i]);
      }
      locked[_to[i]] = lockToken(_amount[i], nowTime, 0, nowTime, false, false);
      tokens = tokens.sub(previousAmount).add(_amount[i]);
    }
    totalVestingTokens = tokens;
    if (tokens > previousTokens) {
      uint256 balanceBefore = token.balanceOf(address(this));
      SafeERC20.safeTransferFrom(
        token,
        msg.sender,
        address(this),
        tokens.sub(previousTokens)
      );
      uint256 _amountReceive = token.balanceOf(address(this)) - balanceBefore;
      require(
        tokens.sub(previousTokens) == _amountReceive,
        "Tokens with a fee on transfer are not supported"
      );
    } else if (tokens < previousTokens) {
      SafeERC20.safeTransfer(token, msg.sender, previousTokens.sub(tokens));
    }

    emit WhiteListAddresses(_to.length, totalVestingTokens);
    return true;
  }

  /**
   * @dev Returns unlockable tokens for a specified address for a specified reason
   * @param _of The address to query the unlockable token count of
   */
  function tokensUnlockable(address _of)
    internal
    view
    returns (
      uint256 amount,
      bool tgeStatus,
      bool totalClaimed
    )
  {
    uint256 _vestingStartTime = vestingStartTime;
    if (_vestingStartTime >= block.timestamp || vestingStartTime == 0) {
      return (0, false, false);
    }
    uint256 _vestingTokens = locked[_of].vestingTokens;
    uint256 _amount = locked[_of].amount;
    if (_vestingStartTime.add(completeVestingPeriod) <= block.timestamp) {
      return (_amount.sub(_vestingTokens), true, true);
    }
    uint256 _monthPeriod = monthPeriod;
    uint256 _cliffTime = cliffTime;
    uint256 _secondDistributionTime = secondDistributionTime;
    uint256 _period = _monthPeriod.add(_cliffTime).add(_secondDistributionTime);
    if (_vestingStartTime.add(_period) <= block.timestamp) {
      uint256 _currentPercent = tgePercent.add(secondDistributionPercent);
      uint256 _releaseToken = _amount.mul(_currentPercent) / 100;
      uint256 _restTotalVesting = _amount.mul(
        uint256(100).sub(_currentPercent)
      ) / 100;
      _releaseToken = _releaseToken.add(
        _restTotalVesting.mul(
          block.timestamp.sub(_vestingStartTime.add(_period))
        ) / completeVestingPeriod.sub(_period)
      );
      return (_releaseToken.sub(_vestingTokens), true, false);
    }
    _period = _monthPeriod.add(_cliffTime);
    if (_vestingStartTime.add(_period) <= block.timestamp) {
      uint256 _releaseToken = _amount.mul(tgePercent) / 100;
      uint256 _restTotalVesting = _amount.mul(secondDistributionPercent) / 100;
      _releaseToken = _releaseToken.add(
        _restTotalVesting.mul(
          block.timestamp.sub(_vestingStartTime.add(_period))
        ) / _secondDistributionTime
      );
      return (_releaseToken.sub(_vestingTokens), true, false);
    }
    if (_vestingStartTime.add(tgeTime) <= block.timestamp) {
      return (
        _amount.mul(tgePercent).div(100).sub(_vestingTokens),
        true,
        false
      );
    }
    return (0, false, false);
  }

  /**
   * @dev Unlocks the unlockable tokens of a specified address
   * @param _of Address of user, claiming back unlockable tokens
   */
  function unlock(address _of) external returns (uint256 unlockableTokens) {
    bool tgeStatus;
    bool claimedStatus;
    require(vestingStatus, "Vesting not start yet");
    (unlockableTokens, tgeStatus, claimedStatus) = tokensUnlockable(_of);
    if (unlockableTokens > 0) {
      locked[_of].lastVestingTime = block.timestamp;
      locked[_of].tgeClaimed = tgeStatus;
      if (claimedStatus) {
        locked[_of].vestingTokens = locked[_of].amount;
        locked[_of].totalVested = claimedStatus;
      } else {
        locked[_of].vestingTokens = locked[_of].vestingTokens.add(
          unlockableTokens
        );
      }
      SafeERC20.safeTransfer(token, _of, unlockableTokens);
    }

    emit Unlock(_of, unlockableTokens);
    return unlockableTokens;
  }

  /**
   * @dev Gets the unlockable tokens of a specified address
   * @param _of The address to user
   */
  function getUnlockableTokens(address _of)
    external
    view
    returns (uint256 unlockableTokens)
  {
    (unlockableTokens, , ) = tokensUnlockable(_of);
    return unlockableTokens;
  }
}

